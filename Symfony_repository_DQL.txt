﻿<?php

// Symfony Reppository for handling filtering/sorting/searching of Question Enteties.
// Using DQL quiries

class QuestionRepository extends AbstractRepository
{

   protected $alias = 'q';
   protected $aliasPrimaryCategory = 'qc1';
   protected $aliasSecondaryCategory = 'qc2';
   protected $aliasProductLevel = 'pl';
   protected $aliasQuestionProductLevel = 'qpl';
   protected $aliasCaseSet = 'qc';
   protected $aliasStatistics = 'stat';

   /**
    * Get filter for the entity
    *
    * @return \PrivateProject\MainBundle\Filter\Filter
    */
   public function getFilter()
   {
      $filterFields = array(
         'id',
         'isDemo',
         'primaryCategory',
         'productLevel',
         'time',
         'secondaryCategory',
         'productLevels',
         'status',
         'score',
         'author',
         'caseSet',
         'createdAt',
         'updatedAt',
         'difficulties',
      );
      $orderFields = array(
         'id',
         'primaryCategory',
         'attachments',
         'secondaryCategory',
         'productLevels',
         'caseSet',
         'createdAt',
         'updatedAt',
         'avgScore',
      );

      $filter = new Filter($filterFields, $orderFields);

      $statusAlias = new FilterAlias('status', 'status');
      $statusAlias->addValue('published', Question::STATUS_PUBLISHED);
      $statusAlias->addValue('pending', Question::STATUS_PENDING);
      $statusAlias->addValue('editing', Question::STATUS_EDITING);
      $statusAlias->addValue('rejected', Question::STATUS_REJECTED);
      $statusAlias->addValue('archived', Question::STATUS_ARCHIVED);
      $filter->addAlias($statusAlias);

      $timeRewrite = new FilterRewrite('time', 'updatedAt');
      $filter->addRewrite($timeRewrite);
      $productLevelRewrite = new FilterRewrite('productLevel', 'productLevels');
      $filter->addRewrite($productLevelRewrite);

      return $filter;
   }

   /**
    * Finds entities by a set of criteria.
    *
    * @param array $criteria
    * @param \PrivateProject\MainBundle\Filter\Filter $filter
    *
    * @return array The objects.
    */
   public function getFiltered(Filter $filter)
   {
      $qb = $this->createQueryBuilder($this->alias);
      $qb->groupBy("$this->alias");

      $this->filter($qb, $filter);
      $this->order($qb, $filter);
      $this->limit($qb, $filter);

      return $qb->getQuery()->getResult();
   }

   /**
    * @param QueryBuilder $qb
    * @param Filter $filter
    * @param string $alias
    * @return QueryBuilder
    */
   protected function order(QueryBuilder $qb, Filter $filter)
   {
      $field = $filter->getOrderBy();
      if (!$field) {
         return $qb;
      }

      switch ($field) {
         case 'avgScore':
            $fieldWithAlias = 'timesCorrect';
            $order = $filter->getOrder();
            $qb->orderBy($fieldWithAlias, $order);
            break;
         case 'attachments':
//                $fieldWithAlias = 'timesCorrect';
//                $order = $filter->getOrder();
//                $qb->orderBy($fieldWithAlias, $order);
            break;
         default:
            $fieldWithAlias = $this->alias . '.' . $field;
            $order = $filter->getOrder();
            $qb->addGroupBy($fieldWithAlias);
            $qb->orderBy($fieldWithAlias, $order);
            break;
      }

      return $qb;
   }

   public function getFilteredQuestions(
      array $productLevels,
      array $qDifficulties = array("all"),
      array $qPrimaryCategories = null,
      array $qSecondaryCategories = null,
      $withCase = null
   ) {
      $questionsQb = $this->createQueryBuilder("q");
      $questionsQb->select(
         "q.id",
         "st.id as productLevelId",
         "IDENTITY(q.caseSet) as caseid",
         "IDENTITY(q.primaryCategory) as category1",
         "IDENTITY(q.secondaryCategory) as category2"
      )
         ->innerJoin("q.peerStat", "qst")
         ->innerJoin("qst.questionProductLevels", "st")
         ->where("qst.productLevel IN (:productLevels)")
         ->setParameter("productLevels", $productLevels);
      if ($qPrimaryCategories !== null) {
         if (array_search("all", $qPrimaryCategories) === false) {
            $questionsQb->andWhere("q.primaryCategory IN (:categories1)")
               ->setParameter("categories1", $qPrimaryCategories);
         }
      }
      if ($qSecondaryCategories !== null) {
         if (array_search("all", $qSecondaryCategories) === false) {
            $questionsQb->andWhere("q.secondaryCategory IN (:categories2)")
               ->setParameter("categories2", $qSecondaryCategories);
         }
      }
      if ($withCase !== null) {
         if ($withCase) {
            $questionsQb->andWhere("q.caseSet IS NOT NULL");
         } else {
            $questionsQb->andWhere("q.caseSet IS NULL");
         }
      }

      $questionsQb->andWhere($questionsQb->expr()->eq("qst.timesTotal", 0));
      $questionsQb->orderBy("q.id");

      $r1 = $questionsQb->getQuery()->getArrayResult();

      $questionsQb = $this->createQueryBuilder("q");
      $questionsQb->select(
         "q.id",
         "st.id as productLevelId",
         "IDENTITY(q.caseSet) as caseid",
         "IDENTITY(q.primaryCategory) as category1",
         "IDENTITY(q.secondaryCategory) as category2"
      )
         ->innerJoin("q.peerStat", "qst")
         ->innerJoin("qst.questionProductLevels", "st")
         ->where("qst.productLevel IN (:productLevels)")
         ->setParameter("productLevels", $productLevels);
      if ($qPrimaryCategories !== null) {
         if (array_search("all", $qPrimaryCategories) === false) {
            $questionsQb->andWhere("q.primaryCategory IN (:categories1)")
               ->setParameter("categories1", $qPrimaryCategories);
         }
      }
      if ($qSecondaryCategories !== null) {
         if (array_search("all", $qSecondaryCategories) === false) {
            $questionsQb->andWhere("q.secondaryCategory IN (:categories2)")
               ->setParameter("categories2", $qSecondaryCategories);
         }
      }
      if ($withCase !== null) {
         if ($withCase) {
            $questionsQb->andWhere("q.caseSet IS NOT NULL");
         } else {
            $questionsQb->andWhere("q.caseSet IS NULL");
         }
      }

      $questionsQb->andWhere($questionsQb->expr()->gt("qst.timesTotal", 0));
      if (array_search("all", $qDifficulties) === false) {
         $conds = $questionsQb->expr()->orX();
         $ranges = array(
            "easy" => array(78.0, 10000),
            "medium" => array(58.0, 78.0),
            "difficult" => array(0, 58.0),
         );
         foreach ($ranges as $difficulty => $range) {
            if (array_search($difficulty, $qDifficulties) !== false) {
               list($min, $max) = $range;

               $conds->add(
                  $questionsQb->expr()->andX(
                     $questionsQb->expr()->gte("qst.timesAvgStat", ":diff{$difficulty}_min"),
                     $questionsQb->expr()->lt("qst.timesAvgStat", ":diff{$difficulty}_max")
                  )
               );
               $questionsQb->setParameter("diff{$difficulty}_min", $min);
               $questionsQb->setParameter("diff{$difficulty}_max", $max);
            }
         }

         $questionsQb->andWhere($conds);
      }
      $questionsQb->orderBy("q.id");

      $r2 = $questionsQb->getQuery()->getResult();

      return array_merge($r1, $r2);
   }

   static public function getIds($a)
   {
      return array_map(
         function ($e) {
            return $e['id'];
         },
         $a
      );
   }
  
   public function getIncorrectQuestionIds(Student $student)
   {
      $em = $this->getEntityManager();
      $qb = $em->createQueryBuilder();
      $qb->select("q.id")
         ->distinct(true)
         ->from(Exam::PATH, "e")
         ->join("e.lastSession", "ls")
         ->join("ls.answers", "eah")
         ->join("eah.examQuestion", "eq")
         ->join("eq.question", "q")
         ->where("e.student = :student")
         ->andWhere($qb->expr()->orX("eah.isCorrect = false", "eah.selectedOption IS NULL"))
         ->setParameter("student", $student)
         ->orderBy("q.id");

      return $this->getIds($qb->getQuery()->getScalarResult());
   }

   public function getMarkedQuestionIds(Student $student)
   {
      $em = $this->getEntityManager();
      $qb = $em->createQueryBuilder();
      $qb->select("q.id as id")
         ->distinct(true)
         ->from(MarkedQuestion::PATH, "mq")
         ->join("mq.question", "q")
         ->where("mq.student = :student")
         ->setParameter("student", $student)
         ->orderBy("q.id");

      return $this->getIds($qb->getQuery()->getScalarResult());
   }

   /**
    * @param Student $student
    * @param array $productLevels
    * @param array $qTypes
    * @param array $qDifficulties
    * @param array $qPrimaryCategories
    * @param array $qSecondaryCategories
    * @param boolean $withCase leave null/out if all questions are needed (part of a case set and standalone)
    * @return array
    */
   public function getStudentQuestions(
      Student $student,
      array $productLevels,
      array $qTypes = null,
      array $qDifficulties = null,
      array $qPrimaryCategories = null,
      array $qSecondaryCategories = null,
      $withCase = null
   ) {

      $questions = $this->getFilteredQuestions(
         $productLevels,
         $qDifficulties,
         $qPrimaryCategories,
         $qSecondaryCategories,
         $withCase
      );

      $questionIds = array_map(
         function ($q) {
            return $q['id'];
         },
         $questions
      );

      $blockedIds = $this->getBlockedQuestionIds($student);
      $usedIds = null;

      foreach ($questions as $key => $question) {
         if (in_array($question['id'], $blockedIds)) {
            $question['isBlocked'] = true;
         } else {
            $question['isBlocked'] = false;
         }
         $questions[$key] = $question;
      }

      $resultIds = array();
      if (array_search("all", $qTypes) === false) {
         if (array_search("new", $qTypes) !== false) {
            $usedIds = $this->getUsedQuestionIds($student);
            $resultIds = array_merge($resultIds, array_diff($questionIds, $usedIds));
         }
         if (array_search("used", $qTypes) !== false) {
            if ($usedIds === null) { // check if it was already initialized for "new"
               $usedIds = $this->getUsedQuestionIds($student);
            }
            $resultIds = array_merge($resultIds, $usedIds);
         }
         if (array_search("incorrect", $qTypes) !== false) {
            $incorrectIds = $this->getIncorrectQuestionIds($student);
            $resultIds = array_merge($resultIds, $incorrectIds);
         }
         if (array_search("marked", $qTypes) !== false) {
            $markedIds = $this->getMarkedQuestionIds($student);
            $resultIds = array_merge($resultIds, $markedIds);
         }
         $resultIds = array_unique($resultIds);
      } else {
         $resultIds = $questionIds;
      }

      $resultQuestions = array();
      foreach ($questions as $q) {
         /**
          * @var Question $q
          */
         if (array_search($q['id'], $resultIds) !== false) {
            $resultQuestions[] = $q;
         }
      }

      return $resultQuestions;
   }

   public function _getidsByIds($ids)
   {
      $em = $this->getEntityManager();
      $qb = $em->createQueryBuilder();
      $qb->select("COUNT(q.id)")
         ->from(Question::PATH, "q")
         ->where("q.id in (:ids)")
         ->setParameter("ids", $ids);

      $result = $qb->getQuery()->getSingleScalarResult();

      return $result;
   }

   private function applyFiltersToQueryBuilder(\Doctrine\ORM\QueryBuilder &$qb, array $filters)
   {
      foreach ($filters as $filter) {
         list($field, $operation, $value) = array_values($filter);

         switch ($field) {
            case "caseSet":
               $qb->andWhere($this->getExpr($qb, "q.caseSet", $operation, $value));
               break;
            case "level":
               $qb->join('q.questionProductLevels', 'qst');
               $qb->join('qst.productLevel', 'st');
               $expr = $qb->expr()->in('st.name', explode(',', $value));
               $qb->andWhere($expr);
               break;
            case "status":
               $qb->andWhere("q.status = :status");
               $qb->setParameter("status", $value);
               break;
            case "primaryCategory":
               $qb->join('q.primaryCategory', 'pc');
               $expr = $qb->expr()->in('q.primaryCategory', explode(',', $value));
               $qb->andWhere($expr);
               break;
            case "secondaryCategory":
               $qb->join('q.secondaryCategory', 'sc');
               $expr = $qb->expr()->in('q.secondaryCategory', explode(',', $value));
               $qb->andWhere($expr);
               break;
            case "author":
               $qb->join("q.author", 'a');
               $qb->andWhere("a.id = :author");
               $qb->setParameter("author", $value);
               break;
            case "createdAt":
               $op = $operation == "min"
                  ? '>'
                  : ($operation == "max"
                     ? '<'
                     : '=');
               $qb->andWhere("s.createdAt $op :createdAt_$operation ");
               $qb->setParameter("createdAt_$operation", $value);
               break;
            case "updatedAt":
               $op = $operation == "min"
                  ? '>'
                  : ($operation == "max"
                     ? '<'
                     : '=');
               $qb->andWhere("s.updatedAt $op :updatedAt_$operation ");
               $qb->setParameter("updatedAt_$operation", $value);
               break;
            case "score":
               $op = $operation == "min"
                  ? '>='
                  : ($operation == "max"
                     ? '<='
                     : '=');
               $qb->andHaving(
                  "COALESCE((100.0 * SUM(cs.timesCorrect) / SUM(cs.timesTotal)),0) $op :avgScore_$operation "
               );
               $qb->setParameter("avgScore_$operation", $value);
               break;
            case "text":
               $qb->andWhere(
                  $qb->expr()->orX(
                     $this->getExpr($qb, "q.description", $operation, $value),
                     $this->getExpr($qb, "q.explanation", $operation, $value),
                     $this->getExpr($qb, "q.takeHomeMessage", $operation, $value),
                     $this->getExpr($qb, "q.diggingDeeperText", $operation, $value),
                     $this->getExpr($qb, "q.referencesText", $operation, $value)
                  )
               );
               break;
            case "productLevel":
               $qb->join('q.questionProductLevels', 'qst');
               $qb->join('qst.productLevel', 'st');
               $expr = $qb->expr()->eq('st.id', $value);
               $qb->andWhere($expr);
               break;
         }
      }
   }

   /**
    * Finds max external id
    *
    * @return integer
    */
   public function findMaxExternalId()
   {
      $qb = $this->createQueryBuilder($this->alias);
      $qb->select("MAX($this->alias.externalId)");

      try {
         $result = $qb->getQuery()->getSingleScalarResult();

         return $result
            ? $result
            : 0;
      } catch (NoResultException $e) {
         return 0;
      }
   }

   /**
    * Update existing record: change status tu PUBLISHED
    *
    * @param ProductLevel $productLevel
    * @return integer rows updated
    */
   public function updateStatusToPublished(ProductLevel $productLevel)
   {
      $questionIds = $this->findIdsByProductLevels($productLevel);
      if (count($questionIds) < 1) {
         return 0;
      }

      // Process Case Sets
      $qb = $this->getEntityManager()->createQueryBuilder();
      $qb->update($this->_entityName, $this->alias);
      $qb->set("$this->alias.status", ':status');
      $qb->where($this->getExpr($qb, "$this->alias.id", FilterOperation::OP_IN, $questionIds));
      $qb->setParameter("status", Question::STATUS_PUBLISHED);
      $rows = $qb->getQuery()->execute();

      return $rows;
   }

   /**
    * Update existing record: change status tu PENDING
    *
    * @param ProductLevel $productLevel
    * @return integer rows updated
    */
   public function updateStatusToPending(ProductLevel $productLevel)
   {
      $questionIds = $this->findIdsByProductLevels($productLevel);
      if (count($questionIds) < 1) {
         return 0;
      }

      // Process Case Sets
      $qb = $this->getEntityManager()->createQueryBuilder();
      $qb->update($this->_entityName, $this->alias);
      $qb->set("$this->alias.status", ':status');
      $qb->where($this->getExpr($qb, "$this->alias.id", FilterOperation::OP_IN, $questionIds));
      $qb->setParameter("status", Question::STATUS_PUBLISHED);
      $rows = $qb->getQuery()->execute();

      return $rows;
   }

   public function findIdsByProductLevels(ProductLevel $productLevel)
   {
      $em = $this->getEntityManager();
      $qb = $em->createQueryBuilder();
      $qb->select("IDENTITY($this->aliasQuestionProductLevel.question) as id");
      $qb->from(QuestionProductLevel::PATH, $this->aliasQuestionProductLevel);
      $qb->where(
         $this->getExpr(
            $qb,
            "$this->aliasQuestionProductLevel.productLevel",
            FilterOperation::OP_EQUAL,
            $productLevel
         )
      );
      $result = $qb->getQuery()->execute();

      $ids = array();
      foreach ($result as $row) {
         if (!$row['id']) {
            continue;
         }
         $ids[] = $row['id'];
      }

      return $ids;
   }


}
